import Crawler as c
import REST as r
import logging as l
import json
import praw

if __name__ == '__main__':
    logger = l.getLogger('[MAIN]')
    logger.setLevel(l.DEBUG)

    l.basicConfig(format='%(levelname)s(%(name)s):%(message)s', level=l.DEBUG)
    cred_file = file("credentials.json")
    credentials = json.load(cred_file)

    reddit = praw.Reddit(client_id = credentials['client_id'],
                         username=credentials['username'],
                         client_secret=credentials['client_secret'],
                         password=credentials['password'],
                         user_agent=credentials['user_agent'])

    crawler = c.Crawler.Crawler("all", reddit)
    crawler.start()

    restapi = r.REST.RESTAPI(crawler)
    restapi.run(host="0.0.0.0", port=5002)


