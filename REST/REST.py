from flask import Flask, jsonify, request
import sqlite3

class RESTAPI(Flask):
    def __init__(self, crawler):
        Flask.__init__(self, import_name=__name__)
        self.crawler = crawler

        @self.route('/topScore', methods=['GET'])
        def getTop10Score():
            conn = sqlite3.connect('db.db')
            cursor = conn.cursor()
            cursor.execute('SELECT * FROM Submissions ORDER BY score desc limit 10')
            results = cursor.fetchall()
            return jsonify(results)

        @self.route('/topComments', methods=['GET'])
        def getTop10Commented():
            conn = sqlite3.connect('db.db')
            cursor = conn.cursor()
            cursor.execute('SELECT * FROM Submissions ORDER BY num_comments desc limit 10')
            results = cursor.fetchall()
            return jsonify(results)

        @self.route('/update/<subm_id>', methods=['GET'])
        def updateSubmission(subm_id):
            return jsonify(self.crawler.updateSubmission(subm_id))

        @self.route('/submissions/<username>', methods=['GET'])
        def getUserSubmissions(username):
            return jsonify(self.crawler.getUserSubmissions(username))

        @self.route('/comments/<username>', methods=['GET'])
        def getUserComments(username):
            return jsonify(self.crawler.getUserComments(username))

        @self.route('/submissions_commented/<username>', methods=['GET'])
        def getSubmissionsCommented(username):
            return jsonify(self.crawler.getSubmissionsCommented(username))
