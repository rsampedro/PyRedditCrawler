PyRedditCrawler
===================
(required packages: praw, Flask, sqlite3)
This script launches a thread that requests periodically the X first submissions ordered in HOT in the reddit frontpage (or subreddit given)  and stores their information on an SQLite  database.

Meanwhile it launches a Flask instance which acts as REST API, with several kinds of request that retrieves from the SQLite database.


Project Structure
--------

**main.py:**
Entry point for this script, launches the crawling script on a different thread and then proceeds to launch a Flask instance that will control the REST API

* Crawler
**Crawler.py**:
Logic of the crawler, from which we'll make the calls to the DB and which will run parallel to the main logic
**RedditSubmission.py**:
Class defining the parameters we want to extract from the submissions we crawl to and whatever functions are needed to transform them so they can be used by either the REST API or anything else.

* REST
**REST.py**:
Code for the Flask instance used as REST API, contains the different requests that can be made and their respective routes

**db.db**:
Sqlite3 database where the information gathered by the crawler is saved.
Contains the tables:

 - Submissions: Info from the submissions gathered

 - Users: (PLACEHOLDER in case I do Bonus 2) (To save information from users)

**credentials.json (NEEDED)**:
File containing the credentials used by this script to connect using the Reddit API. **not included in the repo**

Requests
--------------
 * /topScore - Returns a JSON with the info of the top 10 highest scored submissions
 * /topComments - Returns a JSON with the info of the top 10 most commented submissions
 * /update/< id of the submission > - Updates the given submission on the DB


Design Decisions & Assumptions
------
 * Sqlite3 DB: I picked this database because of its ease of use and because since I'm going to run everything on the same machine I have no use for more powerful DBs.
 * Reddit has very strict policies on the use of Bots and scripts on the platform, limiting the ammount of requests that can be made. They control carefully the user agents of whatever is connecting to the platform and are pretty ban happy with connections that don't make use of the API they provide.
	 * I imported **praw** the wrapper for the reddit API for python, which controls the ammount of requests that can be made to Reddit and helps with authentication ( I registered the script with its own reddit user ) .
	 * This means, that whenever retrieving submissions, we cannot go page by page, but by number of submissions we want ( the default page size on reddit contains 25 submissions )

 * **Bonus 1 (Done):** Given the structure of the script I've used and the information the reddit API provides, implementing it was trivial. The submission given is updated on the db if existing, if it doesn't it adds it. It could also be implemented a command to refresh all the submissions in the DB, but that operation would become more costly with an increasing amount of submissions.
 * **Bonus 2:** This point requires keeping data on the users, since the reddit API doesn't provide this information, which at the same time requires that the crawler script also requests info on every user commenting on every submission to keep track of it, which could easily eat up on the amount of requests we're allowed to make.
 * **Bonus 3 (Done):** For the posts a user has commented in, I only retrieve the ID of the post, since I could easily eat up the requests a bot is allowed to make, even if only requesting the UNIQUE submissions commented on. 
 * **Bonus 4:(1/2)** The first statistic is pretty easy to retrieve, but the 2nd one requires, like in Bonus 2, to retain information on the users seen at the submissions we crawl to, since reddit doesn't provide this information.
 * **Bonus 5:** I haven't tried TravisCI ever and I'm going to skip this point, unless I can manage to squeeze some time for it.