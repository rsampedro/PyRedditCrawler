import threading as t
import logging as l

import RedditSubmission as subm
import RedditComment as comm

import sqlite3


class Crawler(t.Thread):
    def __init__(self, subreddit, connection, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        t.Thread.__init__(self, group=group, target=target, name=name, verbose=verbose)
        self.args = args
        self.kwargs = kwargs
        self.semafor = t.Semaphore(1)
        self.subreddit = subreddit
        self.reddit = connection
        self.logger = l.getLogger('[CRAWLER]')


    def run(self):
        self.logger.debug("Crawler thread started")
        sleep_time = 30
        while True:
            self.crawl(50)
            self.logger.debug("Sleeping for {} seconds".format(sleep_time))
            t._sleep(sleep_time)

    def crawl(self, numthreads):
        self.semafor.acquire()
        subm_list = []
        frontpage = self.reddit.subreddit(self.subreddit).hot(limit=numthreads)
        for thread in frontpage:
            submission = subm.RedditSubmission(thread)
            subm_list.append(submission.getAsTuple())
        self.logger.debug("Add results to DB")
        conn = sqlite3.connect('db.db')
        cursor = conn.cursor()
        cursor.executemany('REPLACE INTO Submissions VALUES (null,?,?,?,?,?,?,?,?,?,?)', subm_list)
        conn.commit()
        self.logger.debug("Added results successfully")
        self.semafor.release()

    def updateSubmission(self, subm_id):
        self.semafor.acquire()
        data = self.reddit.submission(subm_id)
        submission = subm.RedditSubmission(data)
        subm_list = []
        subm_list.append(submission)
        self.logger.debug("Update submission {} in DB".format(subm_id))
        conn = sqlite3.connect('db.db')
        cursor = conn.cursor()
        cursor.execute('REPLACE INTO Submissions VALUES (null,?,?,?,?,?,?,?,?,?,?)', submission.getAsTuple())
        conn.commit()
        self.logger.debug("Updated result successfully")
        self.semafor.release()
        return submission.getAsTuple()

    def returnMedianCommentScore(self, username, subreddit, limit):
        self.semafor.acquire()
        user = self.reddit.redditor(username)
        comment_score = user.comment_karma
        num_comments = sum (1 for item in user.comments.new(limit=None))
        median_score = None
        if num_comments == 0:
            median_score = 0
        else:
            median_score = comment_score/num_comments
        self.semafor.release()
        return median_score

    def getUserSubmissions(self, username):
        posts = []
        self.semafor.acquire()
        user = self.reddit.redditor(username)
        for item in user.submissions.new(limit=None):
            submission = subm.RedditSubmission(item)
            posts.append(submission.getAsTuple())
        self.semafor.release()
        return posts

    def getUserComments(self, username):
        comments = []
        self.semafor.acquire()
        user = self.reddit.redditor(username)
        for item in user.comments.new(limit=None):
            comment = comm.RedditComment(item)
            comments.append(comment.getAsTuple())
        self.semafor.release()
        return comments

    def getSubmissionsCommented(self, username):
        submissions = set()
        self.semafor.acquire()
        user = self.reddit.redditor(username)
        for item in user.comments.new(limit=None):
            comment = comm.RedditComment(item)
            submissions.add(comment.submission_id)
        self.semafor.release()
        return list(submissions)

    def test(self):
        self.logger.debug("Test function")
