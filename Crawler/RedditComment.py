class RedditComment():
    def __init__(self, entry):
        self.id = entry.id
        self.subreddit = entry.subreddit.display_name
        self.score = entry.score
        self.permalink = entry.permalink
        self.submission_id = entry.link_id
        self.creation_date = entry.created_utc

    def getAsTuple(self):
        return (self.id,
                self.subreddit,
                self.score,
                self.permalink,
                self.submission_id,
                self.creation_date)