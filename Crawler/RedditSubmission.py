import praw
import sqlite3

class RedditSubmission():
    def __init__(self, entry):
        self.id = entry.id
        self.title = entry.title
        self.domain = entry.domain
        self.ext_url = entry.url
        self.discussion_url = entry.shortlink
        self.author = entry.author.name
        self.score = entry.score
        self.creation_date = entry.created_utc
        self.num_comments = entry.num_comments
        self.is_discussion = entry.is_self

    def getAsTuple(self):
        return (self.id,
                self.title,
                self.domain,
                self.ext_url,
                self.discussion_url,
                self.author,
                self.score,
                self.creation_date,
                self.num_comments,
                self.is_discussion)
